/*
articles.c
Maxime	POULAIN
Nicolas	SELVY
Marius	CHANDEZE

Projet A1 Janvier 2019
*/
#include "projet.h"

//_____________________ ARTICLES _____________________
Article lire1Article(FILE *flot){
	Article a;
	fscanf(flot,"%d%f%d%*c",&a.idArticle,&a.prix,&a.qte);
	fgets(a.libelle,100,flot);
	a.libelle[strlen(a.libelle)-1]='\0';
	return a;
}

void afficher1Article(Article a){
	printf("%04d\t%5.2f\t%4d\t\t%s\n", a.idArticle,a.prix,a.qte,a.libelle);
}

void afficherNArticle(ListeArticle l){
	if(l==NULL){
		printf("\n");
		return;
	}
	afficher1Article(l->a);
	afficherNArticle(l->suiv);
}

ListeArticle chargementNArticle(ListeArticle l,char fileName[]){
	FILE *flot;
	Article arti;
	ListeArticle list;
	freeList(l);
	list=newListArticle();
	flot=fopen(fileName,"r");
	if(flot==NULL){
		printf("Erreur Fopen en R\n");
		return NULL;
	}
	arti=lire1Article(flot);
	while(!feof(flot)){
		list=insererEnTete(list,arti);
		arti=lire1Article(flot);
	}
	fclose(flot);
	return list;
}

ListeArticle chargementNArticleBIN(ListeArticle l,char fileName[]){
	FILE *flot;
	Article arti;
	int i,nbArticle;
	l=newListArticle();
	flot=fopen(fileName,"rb");
	if(flot==NULL){
		printf("Erreur Fopen en RB\n");
		return NULL;
	}
	fread(&nbArticle,sizeof(int),1,flot);
	//Lecture du fichier binaire par la fin pour que la liste soit dans le bon ordre
	fseek(flot,-sizeof(Article),SEEK_END);
	for(i=0;i<nbArticle;i++){
		fread(&arti,sizeof(Article),1,flot);
		fseek(flot,-2*sizeof(Article),SEEK_CUR);
		l=insererEnTete(l,arti);
	}
	fclose(flot);
	return l;
}

void affichageArticleRupture(ListeArticle listeArticle){
	if(listeArticle==NULL){
		return;
	}
	affichageArticleRupture(listeArticle->suiv);
	if(listeArticle->a.qte==0){
		afficher1Article(listeArticle->a);
	}
}

void afficherArticleGraphique(ListeArticle listeArticle,Config cfg){
	system("chmod 700 Img/tiv");
	system("chmod 700 Img/display.bash");
	system("clear");

	char buf[100];

	printf("╭─────────────────────────────────────────────────────╮\n");
	printf("│"ULTIMA"                      Article                        "RESET"│\n");
	printf("├─────────────────────────────────────────────────────┤\n");
	printf("│ Identifiant : %04d                                  │\n",listeArticle->a.idArticle);
	printf("│ Produit : %-42s│\n",listeArticle->a.libelle);
	
	if(listeArticle->a.qte!=0){

		printf("│ Quantité en stock : %4d                            │\n",listeArticle->a.qte);
	}
	else{
		printf("│ Quantité en stock :"RED" Rupture de stock "RESET"               │\n");

	}
	printf("│ Prix : %-11.2f                                  │\n",listeArticle->a.prix);
	printf("╰─────────────────────────────────────────────────────╯\n");
	
	if(cfg.displayImage==1){
		snprintf(buf, sizeof(buf), "./Img/display.bash %d",listeArticle->a.idArticle);                                   
		system(buf);  

	}
}

void afficherNArticleGraphique(ListeArticle listeArticle,Config cfg){
	int curs,i=0;
	ListeArticle first=listeArticle;
	while(1){
		afficherArticleGraphique(listeArticle,cfg);		
		curs=keyDetec();
		if(curs==67){
			if(listeArticle->suiv!=NULL){
				listeArticle=listeArticle->suiv;
				i++;
			}
		}
		if(curs==68){
			if(i>=0){
				listeArticle=getNemeArticle(first,i);
				i--;
			}
		}
		if(curs=='q'){
			return;
		}
	}
}

ListeArticle ajouter1Article(ListeArticle listeArticle){
	system("clear");
	Article a;
	printf(YEL "Ajout d'un article nouveau : \n\n" RESET);
	printf("Description du produit : ");
	fgets(a.libelle,100,stdin);
	a.libelle[strlen(a.libelle)-1]='\0';
	printf("\n");
	printf("Prix du produit : ");
	scanf("%f",&a.prix);
	printf("\n");
	printf("Quantité en stock : ");
	scanf("%d",&a.qte);
	printf("\n");
	a.idArticle=longueurListe(listeArticle)+1;
	printf("Veuillez placer une image de l'article dans le dossier \"Img\" nommée \"%d.jpg\"\n",a.idArticle );
	listeArticle=insererEnTete(listeArticle,a);
	testTriListeArticle(listeArticle);
	printf(GRE "Article N°%04d ajouté avec succès\n",a.idArticle);
	scanf("%*c");
	return listeArticle;
}


void arrivageDeStock(ListeArticle listeArticle,Commande **tabCmd,int *nbCommande){
	int idArticleArr,augmentation=-1,newQte;
	Booleen articleExiste;
	printf(YEL "Arrivage de stock : \n\n" RESET);
	printf("Identifiant article arrivé : ");
	while(scanf("%d",&idArticleArr)==0){
		scanf("%*c");
		printf(RED "Error\n" RESET );
		scanf("%d%*c",&idArticleArr);
	}
	articleExiste=rechercherIdArticle(listeArticle,idArticleArr);
	while(articleExiste==0){
		printf("L'article N°%d n'existe pas\n", idArticleArr);
		printf("Identifiant article arrivé : ");
		while(scanf("%d",&idArticleArr)==0){
			scanf("%*c");
			printf(RED "Error\n" RESET );
			scanf("%d%*c",&idArticleArr);
		}
		articleExiste=rechercherIdArticle(listeArticle,idArticleArr);
	}
	printf("Arrivage de \"%s\"\n",envoieLibelle(listeArticle,idArticleArr));
	printf("\n");
	printf("Quantité arrivé : ");
	while(scanf("%d",&augmentation)==0 || augmentation<0){
		scanf("%*c");
		printf(RED "Error\n" RESET );
		scanf("%d%*c",&augmentation);
	}
	newQte=diminuerQteNonLivree(tabCmd,nbCommande,idArticleArr,augmentation);
	printf("\n");
	if(newQte>0){
		augmenterLeStockArticle(listeArticle,idArticleArr,newQte);
	}
	scanf("%*c");
	printf(GRE "Arrivage enregistré avec succès\n" RESET);
}
