/*
clients.c
Maxime	POULAIN
Nicolas	SELVY
Marius	CHANDEZE

Projet A1 Janvier 2019
*/
#include "projet.h"

//_____________________ CLIENTS _____________________
Client lire1Client(FILE *flot){
	Client c;
	fscanf(flot,"%d%s%*c",&c.idClient,c.civil);
	fgets(c.nom,100,flot);
	c.nom[strlen(c.nom)-1]='\0';
	fgets(c.prenom,100,flot);
	c.prenom[strlen(c.prenom)-1]='\0';
	fgets(c.adresse,100,flot);
	c.adresse[strlen(c.adresse)-1]='\0';

	return c;
}

void afficher1Client(Client c){
	printf("N°%d\t%s %s %s\t%s\n",c.idClient,c.civil,c.nom,c.prenom,c.adresse);
}

void afficherNClient(Client *tabClient[],int n){
	int i;
	printf(YEL "Clients : \n\n" RESET);
	printf("Client\tNom\t\t\tAdresse\n\n");
	for(i=0;i<n;i++){
		afficher1Client(*tabClient[i]);
	}
}

int chargementNClient(Client *tabClient[],int tMax,char fileName[]){
	FILE *flot;
	Client c;
	int i=0;
	flot=fopen(fileName,"r");
	if(flot==NULL){
		printf("Erreur Fopen en R\n");
		return -1;
	}
	c=lire1Client(flot);
	while(!feof(flot)){
		if(i==tMax){
			printf("Tableau tabClient est pleins\n");
			fclose(flot);
			return -1;
		}
		tabClient[i]=(Client*)malloc(sizeof(Client));
		if(tabClient[i]==NULL){
			printf("Erreur Malloc\n");
			fclose(flot);
			return -1;
		}
		*tabClient[i]=c;
		i++;
		c=lire1Client(flot);
	}
	fclose(flot);
	return i;
}

int chargementNClientBIN(Client *tabClient[],int tMax,char fileName[]){
	int nbClient,i;
	FILE *flot;
	flot=fopen(fileName,"rb");
	if(flot==NULL){
		printf("Error fopen \"%s\" in RB\n",fileName);
		return -1;
	}
	fread(&nbClient,sizeof(int),1,flot);
	if(nbClient>=tMax){
		printf("Tableau tabClient est pleins\n");
		fclose(flot);
	}
	for(i=0;i<nbClient;i++){
		tabClient[i]=(Client*)malloc(sizeof(Client));
		if(tabClient[i]==NULL){
			fclose(flot);
			printf("Error malloc\n");
			return -1;
		}
		fread(tabClient[i],sizeof(Client),1,flot);
	}
	fclose(flot);
	return nbClient;
}

void ajouterClient(Client *tabClient[],int *nbClient,int maxCli,Client cli){
	if(*nbClient>=maxCli){
		printf("Impossible d'ajouter un client pour cause de tableau plein : taille %d\n",maxCli );
	}
	tabClient[*nbClient]=(Client*)malloc(sizeof(Client));
	if(tabClient[*nbClient]==NULL){
		printf("Erreur malloc client N°%d\n",cli.idClient);
		return;
	}
	*tabClient[*nbClient]=cli;
	(*nbClient)++;
}

Client newClient(int nbClient){
	char civilite[30];
	Client c;
	printf(YEL "Création d'un nouveau client : \n\n" RESET);
	printf("Nom : ");
	fgets(c.nom,100,stdin);
	c.nom[strlen(c.nom)-1]='\0';
	toUpperCase(c.nom);
	printf("\n");

	printf("Prenom : ");
	fgets(c.prenom,100,stdin);
	c.prenom[strlen(c.prenom)-1]='\0';
	toUpperCaseFirstLetter(c.prenom);
	printf("\n");

	printf("Civilité (Mme/Mr/NB) : ");
	scanf("%s%*c",civilite);
	toUpperCase(civilite);
	while(strcmp(civilite,"MME")&&strcmp(civilite,"MR")&&strcmp(civilite,"NB")){
		printf(RED "Civilité incorrect\n" RESET);
		printf("Civilité (Mme/Mr/NB) : ");
		scanf("%s%*c",civilite);
		toUpperCase(civilite);
	}
	toUpperCase(civilite);
	strcpy(c.civil,civilite);
	printf("\n");

	printf("Adresse : ");
	fgets(c.adresse,100,stdin);
	c.adresse[strlen(c.adresse)-1]='\0';
	printf("\n");

	c.idClient=nbClient+1;
	printf(GRE "\nClient N°%d enregistré avec succès ! \n" RESET, c.idClient);
	return c;
}

void creationNewClient(Client *tabClient[],int *nbClient,int maxCli){
	Client cli;
	cli=newClient(*nbClient);
	ajouterClient(tabClient,nbClient,maxCli,cli);
}

int rechercherIdClientIndice(Client *tabClient[],int nbClient,int idClientRech,int *trouve){
	int i;
	for(i=0;i<nbClient;i++){
		if(tabClient[i]->idClient==idClientRech){
			*trouve=1;
			return i;
		}
		if(tabClient[i]->idClient>idClientRech){
			*trouve=0;
			return i;
		}
	}
	*trouve=0;
	return nbClient;
}

void supprimerClientMenu(Client *tabClient[],int *nbClient,Commande **tabCommande,int *nbCommande){
	int idClient,indiceClient,indiceCommande,trouve;
	printf(YEL "Suppression d'un client : \n\n" RESET);
	printf("Identifiant de client à supprimer : ");
	while(scanf("%d",&idClient)==0){
		scanf("%*c");
		printf(RED "Error\n" RESET );
		scanf("%d%*c",&idClient);
	}
	indiceClient=rechercherIdClientIndice(tabClient,*nbClient,idClient,&trouve);
	if(trouve==0){
		printf(RED "Client N°%d introuvable\n" RESET,idClient);
		scanf("%*c");
		return;
	}
	printf(RED "\n\tClient \"%s %s\" va être supprimé" RESET ,tabClient[indiceClient]->nom,tabClient[indiceClient]->prenom);
	scanf("%*c");
	supprimer1Client(tabClient,nbClient,indiceClient);
	indiceCommande=rechercherIndiceCommandeParIdClient(tabCommande,*nbCommande,idClient);
	while(indiceCommande!=-1){
		supprimerCommande(tabCommande,nbCommande,indiceCommande);
		indiceCommande=rechercherIndiceCommandeParIdClient(tabCommande,*nbCommande,idClient);
	}
	scanf("%*c");
	printf(GRE "Client N°%d supprimé avec succès\n" RESET ,idClient );
}

void supprimer1Client(Client *tabClient[], int *nbClient,int indice){
	int i;
	free(tabClient[indice]);
	for(i=indice;i<(*nbClient)-1;i++){
		tabClient[i]=tabClient[i+1];
	}
	(*nbClient)--;
}

void getAdresse(char *adresse){
	printf("\nSaisir la nouvelle adresse : ");
	fgets(adresse,100,stdin);
	adresse[strlen(adresse)-1]='\0';
}

void modifierAdresseClient(Client *tabClient[],int nbClient){
	char adresse[100];
	int idClient,indice,trouve;
	printf(YEL "Modification d'adresse : \n\n" RESET);
	printf("Saisir votre identifiant de client : ");
	while(scanf("%d",&idClient)==0){
		scanf("%*c");
		printf(RED "Error\n" RESET );
		scanf("%d%*c",&idClient);
	}
	printf("\n");
	scanf("%*c");
	indice=rechercherIdClientIndice(tabClient,nbClient,idClient,&trouve);
	while(trouve==0){
		printf("Identifiant de client N°%d introuvable\n",idClient);
		printf("Saisir votre identifiant de client : ");
		while(scanf("%d",&idClient)==0){
			scanf("%*c");
			printf(RED "Error\n" RESET );
			scanf("%d%*c",&idClient);
		}
		printf("\n");
		indice=rechercherIdClientIndice(tabClient,nbClient,idClient,&trouve);
	}
	printf("L'adresse de %s %s est actuellement : \"%s\"\n",tabClient[indice]->prenom,tabClient[indice]->nom,tabClient[indice]->adresse);
	getAdresse(adresse);
	strcpy(tabClient[indice]->adresse,adresse);
	printf(GRE "Adresse modifiée avec succès\n" RESET);
}
