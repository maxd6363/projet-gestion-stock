/*
commandes.c
Maxime	POULAIN
Nicolas	SELVY
Marius	CHANDEZE

Projet A1 Janvier 2019
*/
#include "projet.h"

//_____________________ COMMANDES _____________________
Commande lire1Commande(FILE *flot){
	Commande c;
	fscanf(flot,"%d%d%d%d",&c.idCmd,&c.idArticle,&c.idClient,&c.qteNonL);
	return c;
}

void afficher1Commande(Commande c){
	if(c.qteNonL==1){
		printf("N°%04d\t\tN°%04d\t\tN°%04d\t\t%d Unité\n",c.idCmd,c.idArticle,c.idClient,c.qteNonL);
	}
	else{
		printf("N°%04d\t\tN°%04d\t\tN°%04d\t\t%d Unités\n",c.idCmd,c.idArticle,c.idClient,c.qteNonL);
	}
}

void afficherNCommande(Commande **tabCmd ,int n){
	int i;
	printf(YEL "Commandes : \n\n" RESET);
	printf("Commande\tArticle\t\tClient\t\tQuantité\n");
	for(i=0;i<n;i++){
		afficher1Commande(*tabCmd[i]);
	}
}

Commande** chargementNCommande(int *nbCommande,char fileName[]){
	FILE *flot;
	Commande **tabCmd;
	int i;
	flot=fopen(fileName,"r");
	if(flot==NULL){
		printf("Erreur Fopen en R\n");
		return NULL;
	}
	fscanf(flot,"%d%*c",nbCommande);
	tabCmd=(Commande**)malloc(*nbCommande*sizeof(Commande*));
	if(tabCmd==NULL){
		printf("Erruer Malloc\n");
		fclose(flot);
		return NULL;
	}
	for(i=0;i<*nbCommande;i++){
		tabCmd[i]=(Commande*)malloc(sizeof(Commande));
		if(tabCmd[i]==NULL){
			printf("Erruer Malloc\n");
			fclose(flot);
			return NULL;
		}
		*tabCmd[i]=lire1Commande(flot);
	}
	fclose(flot);
	return tabCmd;
}

Commande** chargementNCommandeBIN(int *nbCommande,char fileName[]){
	int i;
	FILE *flot;
	Commande **tabCmd;
	flot=fopen(fileName,"rb");
	if(flot==NULL){
		printf("Error fopen \"%s\" in RB\n",fileName);
		return NULL;
	}
	fread(nbCommande,sizeof(int),1,flot);
	tabCmd=(Commande**)malloc(*nbCommande*sizeof(Commande*));
	if(tabCmd==NULL){
		fclose(flot);
		printf("Error malloc\n");
		return NULL;
	}
	for(i=0;i<*nbCommande;i++){
		tabCmd[i]=(Commande*)malloc(sizeof(Commande));
		if(tabCmd[i]==NULL){
			fclose(flot);
			printf("Error malloc\n");
			return NULL;
		}
		fread(tabCmd[i],sizeof(Commande),1,flot);
	}
	fclose(flot);
	return tabCmd;
}

Commande ** insererCommande(Commande **tabCmd,int *nbCommande,Commande cmdAInserer){
	Commande **newTabCmd;
	(*nbCommande)++;
	newTabCmd=(Commande**)realloc(tabCmd,*nbCommande*sizeof(Commande*));
	if(newTabCmd==NULL){
		printf("Erreur realloc\n");
		return NULL;
	}
	newTabCmd[(*nbCommande)-1]=(Commande*)malloc(sizeof(Commande));
	if(newTabCmd[(*nbCommande)-1]==NULL){
		printf("Erreur malloc\n");
		return NULL;
	}
	*newTabCmd[(*nbCommande)-1]=cmdAInserer;
	return newTabCmd;
}

void resumeCommande(Commande cmd){
	printf("\n");
	printf(YEL "Resumé de commande\n" RESET);
	printf("Commande N°%04d\n",cmd.idCmd);
	printf("Article  N°%04d\n",cmd.idArticle);
	printf("Client   N°%04d\n",cmd.idClient);
	printf("Quantité   %04d\n",cmd.qteNonL);
	printf("\n");
}

Commande** newCommande(int *nbCommande,int *nbClient,int maxCli,Client *tabClient[],ListeArticle listeArticle,Commande **tabCmd){
	int idClient,qteRestante;
	Commande cmd,** newTabCmd;
	printf(YEL "Passage d'une nouvelle commande : \n\n" RESET);
	idClient=partieClientNewCommande(tabClient,nbClient,maxCli);
	system("clear");
	cmd=partieArticleNewCommande(listeArticle,idClient,nbCommande);
	qteRestante=envoieQuantite(listeArticle,cmd.idArticle);
	if(qteRestante>=cmd.qteNonL){
		reduirLeStockArticle(listeArticle,cmd.idArticle,cmd.qteNonL);
	}
	else{
		reduirLeStockArticle(listeArticle,cmd.idArticle,qteRestante);
		cmd.qteNonL=cmd.qteNonL-qteRestante;
		newTabCmd=insererCommande(tabCmd,nbCommande,cmd);
		tabCmd=newTabCmd;
	}
	printf(GRE "Commande N°%d passée avec succès\n" RESET ,cmd.idCmd);
	return tabCmd;
}

int rechercherIndiceCommandeParIdArticle(Commande **tabCmd,int nbCommande,int idArticle){
	int i;
	for(i=0;i<nbCommande;i++){
		if(tabCmd[i]->idArticle==idArticle){
			return i;
		}
	}
	return -1;
}

int rechercherIndiceCommandeParIdClient(Commande **tabCmd,int nbCommande,int idClient){
	int i;
	for(i=0;i<nbCommande;i++){
		if(tabCmd[i]->idClient==idClient){
			return i;
		}
	}
	return -1;
}

void supprimerCommande(Commande **tabCommande, int *nbCommande,int indice){
	int i;
	free(tabCommande[indice]);
	for(i=indice;i<(*nbCommande)-1;i++){
		tabCommande[i]=tabCommande[i+1];
	}
	(*nbCommande)--;
}

int diminuerQteNonLivree(Commande **tabCmd,int *nbCommande,int idArticleArr,int qteArr){
	int indice,newQte=qteArr;
	indice=rechercherIndiceCommandeParIdArticle(tabCmd,*nbCommande,idArticleArr);
	while(indice!=-1 && newQte>0){
		if(tabCmd[indice]->qteNonL<=newQte){
			newQte=newQte-tabCmd[indice]->qteNonL;
			supprimerCommande(tabCmd,nbCommande,indice);
		}
		else{
			tabCmd[indice]->qteNonL=(tabCmd[indice]->qteNonL)-newQte;
			newQte=0;
		}
		indice=rechercherIndiceCommandeParIdArticle(tabCmd,*nbCommande,idArticleArr);
	}
	return newQte;
}
