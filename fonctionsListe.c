/*
fonctionsListe.c
Maxime	POULAIN
Nicolas	SELVY
Marius	CHANDEZE

Projet A1 Janvier 2019
*/
#include "projet.h"

//_____________________ LISTES _____________________
ListeArticle newListArticle(void){
	return NULL;
}

ListeArticle insererEnTete(ListeArticle l,Article a){
	MaillonArticle *m;

	m=(MaillonArticle*)malloc(sizeof(MaillonArticle));
	if(m==NULL){
		printf("Error Malloc\n");
		return NULL;
	}
	m->a=a;
	m->suiv=l;
	return m;
}

Booleen rechercherIdArticle(ListeArticle listeArticle,int idArticle){
	if(listeArticle==NULL){
		return FALSE;
	}
	if(listeArticle->a.idArticle==idArticle){
		return TRUE;
	}
	return rechercherIdArticle(listeArticle->suiv,idArticle);
}

void freeList(ListeArticle l){
	if(l==NULL){
		return;
	}
	freeList(l->suiv);
	free(l);
}

char* envoieLibelle(ListeArticle listeArticle,int idArticle){
	if(listeArticle==NULL){
		return "";
	}
	if(listeArticle->a.idArticle==idArticle){
		return listeArticle->a.libelle;
	}
	return envoieLibelle(listeArticle->suiv,idArticle);
}

float envoiePrix(ListeArticle listeArticle,int idArticle){
	if(listeArticle==NULL){
		return 0.0;
	}
	if(listeArticle->a.idArticle==idArticle){
		return listeArticle->a.prix;
	}
	return envoiePrix(listeArticle->suiv,idArticle);
}

int envoieQuantite(ListeArticle listeArticle,int idArticle){
	if(listeArticle==NULL){
		return 0.0;
	}
	if(listeArticle->a.idArticle==idArticle){
		return listeArticle->a.qte;
	}
	return envoieQuantite(listeArticle->suiv,idArticle);
}

void reduirLeStockArticle(ListeArticle listeArticle,int idArticle,int reduction){
	if(listeArticle==NULL){
		return;
	}
	if(listeArticle->a.idArticle==idArticle){
		listeArticle->a.qte=listeArticle->a.qte-reduction;
		return;
	}
	return reduirLeStockArticle(listeArticle->suiv,idArticle,reduction);
}

void augmenterLeStockArticle(ListeArticle listeArticle,int idArticle,int augmentation){
	if(listeArticle==NULL){
		return;
	}
	if(listeArticle->a.idArticle==idArticle){
		listeArticle->a.qte=listeArticle->a.qte+augmentation;
		return;
	}
	return augmenterLeStockArticle(listeArticle->suiv,idArticle,augmentation);
}

int longueurListe(ListeArticle l){
	if(l==NULL){
		return 0;
	}
	return longueurListe(l->suiv)+1;
}

ListeArticle getNemeArticle(ListeArticle listeArticle,int i){
	int j;
	for(j=0;j<i;j++){
		listeArticle=listeArticle->suiv;
	}
	return listeArticle;
}