/*
projet.c
Maxime	POULAIN
Nicolas	SELVY
Marius	CHANDEZE

Projet A1 Janvier 2019
*/
#include "projet.h"

//_____________________ MENU _____________________
void dispMenu(void){

	system("clear");
	printf(RESET);
	fflush(stdout);
	fflush(stdin);
	system("date \"+Heure : %k:%M\"");
	system("date \"+Date  : %D\"");

	printf(CYA " Menu Principal\n" RESET);
	printf("╭───────────────────────────────────────────────╮\n");
	printf("│                                               │\n");
	printf("│            Projet Gestion des Stocks          │\n");
	printf("│                                               │\n");
	printf("│   1   Charger Fichiers                        │\n");
	printf("│   2   Affichage                               │\n");
	printf("│   3   Nouvelle  Commande                      │\n");
	printf("│   4   Ajouter Nouvel Article                  │\n");
	printf("│   5   Arrivage  Stock                         │\n");
	printf("│   6   Nouveau  Client                         │\n");
	printf("│   7   Supprimer Client                        │\n");
	printf("│   8   Modifier  Adresse                       │\n");
	printf("│   A   Sauvegarder Fichiers                    │\n");
	printf("│                                               │\n");
	printf("│                                               │\n");
	printf("│   9   Quitter                                 │\n");
	printf("│                                               │\n");
	printf("╰───────────────────────────────────────────────╯\n");
	printf("\n");
}

char choixMenu(void){
	char choix;
	dispMenu();
	printf("Choix : ");
	choix=(char)keyDetec();
	while(!((choix>='1'&&choix<='9')||choix=='A'||choix=='a')){
		printf(RED " Error\n" RESET);
		usleep(ERROR_SLEEP);
		dispMenu();
		printf("Choix : ");
		choix=(char)keyDetec();
	}
	toUpperCase(&choix);
	return choix;
}

void dispMenuChargement(void){

	system("clear");
	printf(RESET);
	fflush(stdout);
	fflush(stdin);
	system("date \"+Heure : %k:%M\"");
	system("date \"+Date  : %D\"");

	printf(CYA " Menu Principal > Charger Fichiers\n" RESET);
	printf("╭───────────────────────────────────────────────╮\n");
	printf("│                                               │\n");
	printf("│            Projet Gestion des Stocks          │\n");
	printf("│                                               │\n");
	printf("│   1   Articles                                │\n");
	printf("│   2   Clients                                 │\n");
	printf("│   3   Commandes                               │\n");
	printf("│   4   Articles Binaire                        │\n");
	printf("│   5   Clients Binaire                         │\n");
	printf("│   6   Commandes Binaire                       │\n");
	printf("│   7   Configuration                           │\n");
	printf("│                                               │\n");
	printf("│                                               │\n");
	printf("│                                               │\n");
	printf("│                                               │\n");
	printf("│   9   Retour                                  │\n");
	printf("│                                               │\n");
	printf("╰───────────────────────────────────────────────╯\n");
	printf("\n");
}

char choixMenuChargement(void){
	char choix;
	dispMenuChargement();
	printf("Choix : ");
	choix=(char)keyDetec();
	while((choix<'1'||choix>'7')&&choix!='9'){
		printf(RED "Error\n" RESET);
		usleep(ERROR_SLEEP);
		dispMenuChargement();
		printf("Choix : ");
		choix=(char)keyDetec();
	}
	return choix;
}

void dispMenuAffichage(void){

	system("clear");
	printf(RESET);
	fflush(stdout);
	fflush(stdin);
	system("date \"+Heure : %k:%M\"");
	system("date \"+Date  : %D\"");

	printf(CYA " Menu Principal > Affichage\n" RESET);
	printf("╭───────────────────────────────────────────────╮\n");
	printf("│                                               │\n");
	printf("│            Projet Gestion des Stocks          │\n");
	printf("│                                               │\n");
	printf("│   1   Articles Classique                      │\n");
	printf("│   2   Articles                                │\n");
	printf("│   3   Articles en rupture de stock            │\n");
	printf("│   4   Clients Classique                       │\n");
	printf("│   5   Clients                                 │\n");
	printf("│   6   Clients Avec Commandes                  │\n");
	printf("│   7   Commandes en cours pour un client       │\n");
	printf("│   8   Commandes                               │\n");
	printf("│                                               │\n");
	printf("│                                               │\n");
	printf("│                                               │\n");
	printf("│   9   Retour                                  │\n");
	printf("│                                               │\n");
	printf("╰───────────────────────────────────────────────╯\n");
	printf("\n");
}

char choixMenuAffichage(void){
	char choix;
	dispMenuAffichage();
	printf("Choix : ");
	choix=(char)keyDetec();
	while((choix<'1'||choix>'8')&&choix!='9'){
		printf(RED "Error\n" RESET);
		usleep(ERROR_SLEEP);
		dispMenuAffichage();
		printf("Choix : ");
		choix=(char)keyDetec();
	}
	return choix;
}

void dispMenuSave(void){

	system("clear");
	printf(RESET);
	fflush(stdout);
	fflush(stdin);	system("date \"+Heure : %k:%M\"");
	system("date \"+Date  : %D\"");
	
	printf(CYA " Menu Principal > Sauvegarder Fichiers\n" RESET);
	printf("╭───────────────────────────────────────────────╮\n");
	printf("│                                               │\n");
	printf("│            Projet Gestion des Stocks          │\n");
	printf("│                                               │\n");
	printf("│   1   Articles texte                          │\n");
	printf("│   2   Clients texte                           │\n");
	printf("│   3   Commandes texte                         │\n");
	printf("│   4   Articles binaire                        │\n");
	printf("│   5   Clients binaire                         │\n");
	printf("│   6   Commandes  binaire                      │\n");
	printf("│   7   Édition Configuration                   │\n");
	printf("│                                               │\n");
	printf("│                                               │\n");
	printf("│                                               │\n");
	printf("│                                               │\n");
	printf("│   9   Quitter                                 │\n");
	printf("│                                               │\n");
	printf("╰───────────────────────────────────────────────╯\n");
	printf("\n");
}

char choixMenuSave(void){
	char choix;
	dispMenuSave();
	printf("Choix : ");
	choix=(char)keyDetec();
	while((choix<'1'||choix>'7')&&choix!='9'){
		printf(RED "Error\n" RESET);
		usleep(ERROR_SLEEP);
		dispMenuSave();
		printf("Choix : ");
		choix=(char)keyDetec();
	}
	return choix;
}

//_____________________ ASCII _____________________

void dispStart(void){
	system("clear");
	printf(RESET);
	fflush(stdout);
	fflush(stdin);
	//system("paplay StartUpSound.ogg &");
	FILE *flot;
	char c;
	flot=fopen("ASCII","r");
	if(flot==NULL){
		printf("Error ASCII Start Up\n");
		return;
	}
	printf("\v\v\v\v");
	printf(REDB);
	fscanf(flot,"%c",&c);
	while(!feof(flot)){
		if(c=='%'){
			printf("\t\t\t\t\t\t\t\t");
		}
		else if(c=='@'){
			usleep(500000);
			printf(YELB);
		}
		else if(c=='$'){
			usleep(500000);
			printf(GREB);
		}
		else{
			printf("%c", c);
		}
		fscanf(flot,"%c",&c);
		usleep(600);
	}
	printf(RESET);
	fclose(flot);
	usleep(500000);
}
void alexaJeNaiPasComprisVotreRequete(void){
	FILE *flot;
	char c;
	flot=fopen("ASCII2","r");
	if(flot==NULL){
		printf("Erreur lors de l'ouverture du fichier en mode r\n");
		return;
	}
	fscanf(flot,"%c",&c);
	while(!feof(flot)){
		if(c=='~'){
			printf(ORAB);

		}
		else{
			printf("%c",c );

		}
		fscanf(flot,"%c",&c);

	}
	fclose(flot);
	usleep(1500000);
	printf(RESET);
}

//_____________________ CONFIG _____________________

Config loadConfigFile(void){
	Config cfg;
	FILE *flot;
	*(cfg.fileNameArticles)='\0';
	*(cfg.fileNameClients)='\0';
	*(cfg.fileNameCommandes)='\0';
	*(cfg.BINfileNameArticles)='\0';
	*(cfg.BINfileNameClients)='\0';
	*(cfg.BINfileNameCommandes)='\0';

	flot=fopen("config.cfg","r");
	if(flot==NULL){
		printf("Error fopen \"config.cfg\" in R\n");
		return cfg;
	}

	fscanf(flot,"%s",cfg.fileNameArticles);
	fscanf(flot,"%s",cfg.fileNameClients);
	fscanf(flot,"%s",cfg.fileNameCommandes);
	fscanf(flot,"%s",cfg.BINfileNameArticles);
	fscanf(flot,"%s",cfg.BINfileNameClients);
	fscanf(flot,"%s",cfg.BINfileNameCommandes);
	fscanf(flot,"%d",(int*)&cfg.startUpAnimation);
	fscanf(flot,"%d",(int*)&cfg.displayImage);
	fclose(flot);
	return cfg;
}

Config editConfig(Config cfg){
	char choix;
	printf(YEL "\nEdition du fichier de configuration : \n\n" RESET);
	printf("Saisir le nom du fichier d'articles : ");
	scanf("%s",cfg.fileNameArticles);
	printf("\n");
	printf("Saisir le nom du fichier de clients : ");
	scanf("%s",cfg.fileNameClients);
	printf("\n");
	printf("Saisir le nom du fichier de commandes : ");
	scanf("%s",cfg.fileNameCommandes);
	printf("\n\n");

	printf("Saisir le nom du fichier binaire d'articles : ");
	scanf("%s",cfg.BINfileNameArticles);
	printf("\n");
	printf("Saisir le nom du fichier binaire de clients : ");
	scanf("%s",cfg.BINfileNameClients);
	printf("\n");
	printf("Saisir le nom du fichier binaire de commandes : ");
	scanf("%s",cfg.BINfileNameCommandes);
	printf("\n\n");

	printf("Animation au démarrage (y/n): ");
	scanf("%c%*c",&choix);
	if(choix=='y'){
		cfg.startUpAnimation=1;
	}
	else{
		cfg.startUpAnimation=0;
	}
	printf("\n");

	printf("Afficher les images des articles (y/n): ");
	scanf("%c%*c",&choix);
	if(choix=='y'){
		cfg.displayImage=1;
	}
	else{
		cfg.displayImage=0;
	}
	printf("\n");
	if(saveConfig(cfg)==0){
		printf(GRE "\nFichier de configuration édité avec succès\n" RESET);
	}
	return cfg;
}

int saveConfig(Config cfg){
	FILE *flot;
	flot=fopen("config.cfg","w");
	if(flot==NULL){
		printf("Erreur fopen \"config.cfg\" en W\n");
		return -1;
	}
	fprintf(flot, "%s\n",cfg.fileNameArticles);
	fprintf(flot, "%s\n",cfg.fileNameClients);
	fprintf(flot, "%s\n",cfg.fileNameCommandes);

	fprintf(flot, "%s\n",cfg.BINfileNameArticles);
	fprintf(flot, "%s\n",cfg.BINfileNameClients);
	fprintf(flot, "%s\n",cfg.BINfileNameCommandes);

	fprintf(flot, "%d\n",cfg.startUpAnimation);
	fprintf(flot, "%d\n",cfg.displayImage);
	fclose(flot);
	return 0;
}

//_____________________ TRI _____________________

//tri dico par idClient tabCommande
void triDicoCommande(Commande **tabCommande,int nbCommande){
	Commande **R,**S;
	if(nbCommande==1){
		return;
	}
	R=(Commande**)malloc((nbCommande/2)*sizeof(Commande*));
	S=(Commande**)malloc((nbCommande-(nbCommande/2))*sizeof(Commande*));
	if(R==NULL || S==NULL){
		printf("Erreur Malloc Dico\n");
		return;
	}
	copier(tabCommande,0,nbCommande/2,R);
	copier(tabCommande,nbCommande/2,nbCommande,S);
	triDicoCommande(R,nbCommande/2);
	triDicoCommande(S,nbCommande-(nbCommande/2));
	fusion(R,nbCommande/2,S,nbCommande-nbCommande/2,tabCommande);
	free(R);
	free(S);
}

void copier(Commande **tabCommande,int n,int m,Commande **RS){
	int i=0;
	while(n<m){
		RS[i]=tabCommande[n];
		i++;
		n++;
	}
}

void fusion(Commande **R,int n,Commande **S,int m,Commande **tabCommande){
	int i=0,j=0,k=0;
	while(i<n && j<m){
		if(R[i]->idClient < S[j]->idClient){
			tabCommande[k]=R[i];
			i++;
			k++;
		}
		else{
			tabCommande[k]=S[j];
			j++;
			k++;
		}

	}
	while(i<n){
		tabCommande[k]=R[i];
		i++;
		k++;
	}
	while(j<m){
		tabCommande[k]=S[j];
		j++;
		k++;
	}
}

// tri La liste des articles par libelle
void testTriListeArticle(ListeArticle listeArticle){
	while(1){
		if(listeArticleTrieHomeMade(listeArticle)==TRUE){
			return;
		}
	}
}

Booleen listeArticleTrieHomeMade(ListeArticle listeArticle){
	Article aux;
	if(listeArticle==NULL){
		return TRUE;
	}
	if(listeArticle->suiv!=NULL){
		if(strcmp(listeArticle->a.libelle,listeArticle->suiv->a.libelle)>0){
			aux=listeArticle->a;
			listeArticle->a=listeArticle->suiv->a;
			listeArticle->suiv->a=aux;
			return FALSE;
		}
	}
	return listeArticleTrieHomeMade(listeArticle->suiv);
}

//_____________________ SAUVEGARDE _____________________

void saveArticle(char fileName[],ListeArticle listeArticle){
	FILE *flot;
	flot=fopen(fileName,"w");
	if(flot==NULL){
		printf("Erreur fopen de \"%s\" en W\n",fileName);
		return;
	}
	while(listeArticle!=NULL){
		fprintf(flot, "%04d\t%.2f\t%d\t%s\n",listeArticle->a.idArticle,listeArticle->a.prix,listeArticle->a.qte,listeArticle->a.libelle);
		listeArticle=listeArticle->suiv;
	}
	fclose(flot);
	printf(GRE "\"%s\" Sauvegardé avec succès\n" RESET, fileName);
}

void saveClient(char fileName[],Client *tabClient[],int nbClient){
	int i;
	FILE *flot;
	flot=fopen(fileName,"w");
	if(flot==NULL){
		printf("Erreur fopen de \"%s\" en W\n",fileName);
		return;
	}
	for(i=0;i<nbClient;i++){
		fprintf(flot, "%04d\n%s\n%s\n%s\n%s\n",tabClient[i]->idClient,tabClient[i]->civil,tabClient[i]->nom,tabClient[i]->prenom,tabClient[i]->adresse);
	}
	fclose(flot);
	printf(GRE "\"%s\" Sauvegardé avec succès\n" RESET, fileName);
}

void saveCommande(char fileName[],Commande **tabCommande,int nbCommande){
	int i;
	FILE *flot;
	flot=fopen(fileName,"w");
	if(flot==NULL){
		printf("Erreur fopen de \"%s\" en W\n",fileName);
		return;
	}
	fprintf(flot, "%d\n",nbCommande);
	for(i=0;i<nbCommande;i++){
		fprintf(flot, "%04d\t%04d\t%04d\t%d\n",tabCommande[i]->idCmd,tabCommande[i]->idArticle,tabCommande[i]->idClient,tabCommande[i]->qteNonL);
	}
	fclose(flot);
	printf(GRE "\"%s\" Sauvegardé avec succès\n" RESET, fileName);
}


void saveArticleBin(char fileName[],ListeArticle listeArticle){
	FILE *flot;
	int nb;
	flot=fopen(fileName,"wb");
	if(flot==NULL){
		printf("Erreur fopen de \"%s\" en WB\n",fileName);
		return;
	}
	nb=longueurListe(listeArticle);
	fwrite(&nb,sizeof(int),1,flot);
	while(listeArticle!=NULL){
		fwrite(&listeArticle->a,sizeof(Article),1,flot);
		listeArticle=listeArticle->suiv;
	}
	fclose(flot);
	printf(GRE "\"%s\" Sauvegardé avec succès\n" RESET, fileName);
}

void saveClientBin(char fileName[],Client *tabClient[],int nbClient){
	int i;
	FILE *flot;
	flot=fopen(fileName,"wb");
	if(flot==NULL){
		printf("Erreur fopen de \"%s\" en WB\n",fileName);
		return;
	}
	fwrite(&nbClient,sizeof(int),1,flot);
	for(i=0;i<nbClient;i++){
		fwrite(tabClient[i],sizeof(Client),1,flot);
	}	
	fclose(flot);
	printf(GRE "\"%s\" Sauvegardé avec succès\n" RESET, fileName);
}

void saveCommandeBin(char fileName[],Commande **tabCommande,int nbCommande){
	int i;
	FILE *flot;
	flot=fopen(fileName,"wb");
	if(flot==NULL){
		printf("Erreur fopen de \"%s\" en WB\n",fileName);
		return;
	}
	fwrite(&nbCommande,sizeof(int),1,flot);
	for(i=0;i<nbCommande;i++){
		fwrite((tabCommande[i]),sizeof(Commande),1,flot);
	}
	fclose(flot);
	printf(GRE "\"%s\" Sauvegardé avec succès\n" RESET, fileName);
}

//_____________________ GESTION DES STOCKS _____________________

void lignesCommandesPourClient(Client *tabClient[],int nbClient,Commande **tabCommande,int nbCommande,ListeArticle listeArticle){
	int idClientRech,trouve,indice,nb;
	printf(YEL "Commandes pour un Clients : \n\n" RESET);
	printf("Saisir votre numéro de client : ");
	while(scanf("%d",&idClientRech)==0){
		scanf("%*c");
		printf(RED "Error\n" RESET );
		scanf("%d%*c",&idClientRech);
	}
	indice=rechercherIdClientIndice(tabClient,nbClient,idClientRech,&trouve);
	while(trouve==0){
		printf("Client N°%d introuvable\n",idClientRech);
		printf("Saisir votre numéro de client : ");
		while(scanf("%d",&idClientRech)==0){
			scanf("%*c");
			printf(RED "Error\n" RESET );
			scanf("%d%*c",&idClientRech);
		}		
		indice=rechercherIdClientIndice(tabClient,nbClient,idClientRech,&trouve);
	}
	afficher1Client(*tabClient[indice]);
	nb=afficherCommandePour1Client(idClientRech,tabCommande,nbCommande,listeArticle);
	if(nb==0){
		printf(RED "  ► Pas de commande pour ce client\n" RESET );
	}
	scanf("%*c");
}

int afficherCommandePour1Client(int idClient,Commande **tabCommande,int nbCommande,ListeArticle listeArticle){
	int i,cpt=0;
	char libelle[100];
	for(i=0;i<nbCommande;i++){



		if(tabCommande[i]->idClient==idClient){
			strcpy(libelle,envoieLibelle(listeArticle,tabCommande[i]->idArticle));
			printf(GRE "  ► N°%d\t\tArticle : %d\t\tQuantité : %d\t%s\n" RESET,tabCommande[i]->idCmd,tabCommande[i]->idArticle,tabCommande[i]->qteNonL,libelle);
			cpt++;
		}
		if(tabCommande[i]->idClient>idClient){
			return cpt;
		}
	}
	return cpt;
}

void affichageClientsAvecCommande(Client *tabClient[],int nbClient,Commande **tabCommande,int nbCommande,ListeArticle listeArticle){
	int i,nb;
	printf(YEL "Commandes par Clients : \n\n" RESET);
	for(i=0;i<nbClient;i++){
		afficher1Client(*tabClient[i]);
		nb=afficherCommandePour1Client(tabClient[i]->idClient,tabCommande,nbCommande,listeArticle);
		if(nb==0){
			printf(RED "  ► Pas de commande pour ce client\n" RESET);
		}
		printf("\n");
	}
}

int partieClientNewCommande(Client *tabClient[],int *nbClient,int maxCli){
	char reponse[10];
	int trouve,idClientRech,indice;
	Client cli;
	printf("Etes vous déjà client (OUI/NON) : ");
	scanf("%s%*c",reponse);
	toUpperCase(reponse);
	if(strcmp(reponse,"NON")==0){
		cli=newClient(*nbClient);
		ajouterClient(tabClient,nbClient,maxCli,cli);
		idClientRech=cli.idClient;
		printf("\n");
		indice=rechercherIdClientIndice(tabClient,*nbClient,idClientRech,&trouve);
	}
	else{
		printf("\nSaisir votre identifiant de client : ");
		while(scanf("%d",&idClientRech)==0){
			scanf("%*c");
			printf(RED "Error\n" RESET );
			scanf("%d%*c",&idClientRech);
		}
		printf("\n");
		indice=rechercherIdClientIndice(tabClient,*nbClient,idClientRech,&trouve);
		while(trouve!=1){
			printf("Le client d'identifiant N°%d n'existe pas\n", idClientRech);
			printf("\n\nSaisir votre identifiant de client : ");
			while(scanf("%d",&idClientRech)==0){
				scanf("%*c");
				printf(RED "Error\n" RESET );
				scanf("%d%*c",&idClientRech);
			}
			printf("\n");
			indice=rechercherIdClientIndice(tabClient,*nbClient,idClientRech,&trouve);
		}
	}
	printf("Bonjour %s %s !\n",tabClient[indice]->prenom,tabClient[indice]->nom);
	usleep(WELCOME_SLEEP);
	printf("\n");
	return tabClient[indice]->idClient;
}

Commande partieArticleNewCommande(ListeArticle listeArticle,int idClient,int *nbCommande){
	int idArticleCommander,qteArticleCommander,qteArticleRestant;
	char reponse[10];

	Commande cmd;
	cmd.idCmd=-1;
	cmd.idArticle=-1;
	cmd.idClient=-1;
	cmd.qteNonL=-1;
	printf(YEL "Passage d'une nouvelle commande : \n\n" RESET);
	printf("N° \t Prix \tQuantité\tLibelle\n");
	afficherNArticle(listeArticle);
	printf("\nNuméro d'article à commander : ");
	while(scanf("%d",&idArticleCommander)==0){
		scanf("%*c");
		printf(RED "Error\n" RESET );
		scanf("%d%*c",&idArticleCommander);
	}
	printf("\n");
	while(rechercherIdArticle(listeArticle,idArticleCommander)==0){
		printf("Article N°%d n'est pas dans la liste des articles proposés\n",idArticleCommander);
		printf("Numéro d'article à commander : ");
		while(scanf("%d",&idArticleCommander)==0){
			scanf("%*c");
			printf(RED "Error\n" RESET );
			scanf("%d%*c",&idArticleCommander);
		}		
		printf("\n");
	}
	printf("Voulez vous commander l'article  \"%s\" à un prix de %.2f €",envoieLibelle(listeArticle,idArticleCommander),envoiePrix(listeArticle,idArticleCommander));
	printf(" (OUI/NON) : ");
	scanf("%s",reponse);
	printf("\n");
	toUpperCase(reponse);
	if(strcmp("NON",reponse)==0){
		return cmd;
	}
	printf("Quantité à commander : ");
	while(scanf("%d",&qteArticleCommander)==0){
		scanf("%*c");
		printf(RED "Error\n" RESET);
		scanf("%d%*c",&qteArticleCommander);
	}
	printf("\n");
	while(qteArticleCommander<=0){
		printf("Vous devez commander au moins un article\n");
		printf("Quantité à commander : ");
		while(scanf("%d",&qteArticleCommander)==0){
			scanf("%*c");
			printf(RED "Error\n" RESET );
			scanf("%d%*c",&qteArticleCommander);
		}
		printf("\n");
	}
	cmd.idCmd=*nbCommande+1;
	cmd.idArticle=idArticleCommander;
	cmd.idClient=idClient;
	cmd.qteNonL=qteArticleCommander;

	qteArticleRestant=envoieQuantite(listeArticle,idArticleCommander);
	printf("Quantité restante en stock : %d\n",qteArticleRestant);
	if(qteArticleRestant-qteArticleCommander<0){
		printf(RED "Votre commande est en attente pour cause de rupture de stock\n" RESET);
	}
	resumeCommande(cmd);
	printf("Le prix à payer est : %.2f\n", calculPrixCmd(envoiePrix(listeArticle,idArticleCommander),qteArticleCommander));

	scanf("%*c");
	return cmd;
}

float calculPrixCmd(float prix,int qte){
	return prix*qte;
}

void resumeClient(Client *tabClient[],Commande **tabCommande,int nbCommande,ListeArticle listeArticle,int indiceClient){
	int i;
	system("clear");
	printf("╭─────────────────────────────────────────────────────╮\n");
	printf("│"ULTIMA"                       Client                        "RESET"│\n");
	printf("├─────────────────────────────────────────────────────┤\n");
	printf("│ Identifiant : %4d                                  │\n",tabClient[indiceClient]->idClient);
	printf("│ Nom : %-43s   │\n",tabClient[indiceClient]->nom);
	printf("│ Prenom : %-43s│\n",tabClient[indiceClient]->prenom);
	if(strcmp(tabClient[indiceClient]->civil,"MR")==0)
		printf("│ Civilité : Monsieur                                 │\n");
	else if (strcmp(tabClient[indiceClient]->civil,"MME")==0)
		printf("│ Civilité : Madame                                   │\n");
	else 
		printf("│ Civilité : Autre                                    │\n");
	if(rechercherIndiceCommandeParIdClient(tabCommande,nbCommande,tabClient[indiceClient]->idClient)!=-1){
		printf("├─────────────────────────────────────────────────────┤\n");
		printf("│"ULTIMA"                     Commandes                       "RESET"│\n");
		printf("├─────────────────────────────────────────────────────┤\n");
		printf("│ Article                       Quantité      Prix    │\n");
		for(i=0;i<nbCommande;i++){
			if(tabClient[indiceClient]->idClient==tabCommande[i]->idClient){
				printf("│ %-35s%3d  %8.2f €  │\n",envoieLibelle(listeArticle,tabCommande[i]->idArticle),tabCommande[i]->qteNonL,envoiePrix(listeArticle,tabCommande[i]->idArticle)*tabCommande[i]->qteNonL);
			}
		}
	}
	printf("╰─────────────────────────────────────────────────────╯\n");
}

void resumeNClient(Client *tabClient[],int nbClient,Commande **tabCommande,int nbCommande,ListeArticle listeArticle){
	int i=0,curs;
	while(1){
		resumeClient(tabClient,tabCommande,nbCommande,listeArticle,i);
		curs=keyDetec();
		if(curs==67){
			if(i<nbClient-1){
				i++;		
			}
		}
		if(curs==68){
			if(i!=0){
				i--;
			}
		}
		if(curs=='q'){
			return;
		}
	}
}

int keyDetec(void){
	int c;   
	static struct termios oldt, newt;
	tcgetattr( STDIN_FILENO, &oldt);
	newt = oldt;
	newt.c_lflag &= ~(ICANON);          
	tcsetattr( STDIN_FILENO, TCSANOW, &newt);
	c=getchar();
	tcsetattr( STDIN_FILENO, TCSANOW, &oldt);
	return c;
}

//_____________________ CHIANES DE CARACTERES _____________________

void toUpperCase(char chaine[]){
	int i=0;
	while(chaine[i]!='\0'){
		if((int)chaine[i]>96 &&(int)chaine[i]<123){
			chaine[i]=(int)chaine[i]-32;
		}
		i++;
	}
}

//Met la première lettre de chaine en majuscule et le reste des caractères en minuscules
void toUpperCaseFirstLetter(char chaine[]){
	if((int)*chaine>=97 &&(int)*chaine<=122){
		*chaine=(int)*chaine-32;
	}
	int i=1;
	while(chaine[i]!='\0'){

		if((int)chaine[i]>=65 &&(int)chaine[i]<=90){
			chaine[i]=(int)chaine[i]+32;
		}
		i++;
	}
}
