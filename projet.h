/*
projet.h
Maxime	POULAIN
Nicolas	SELVY
Marius	CHANDEZE

Projet A1 Janvier 2019
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>


#define RED     "\x1b[31m"
#define GRE		"\x1b[32m"
#define YEL		"\x1b[33m"
#define BLU	    "\x1b[34m"
#define MAG		"\x1b[35m"
#define CYA   	"\x1b[36m"
#define REDB    "\x1b[1;31m"
#define GREB	"\x1b[6;32m"
#define YELB	"\x1b[1;33m"
#define ORAB	"\x1b[3;33m"
#define BLUB	"\x1b[1;34m"
#define MAGB	"\x1b[4;35m"
#define CYAB   	"\x1b[1;36m"
#define BLINK 	"\x1b[5;29m"
#define ULTIMA	"\x1b[3;1;29m"


#define RESET   "\x1b[0m"

#define MAXCLI 800
#define ERROR_SLEEP 500000
#define WELCOME_SLEEP 1000000

typedef enum{FALSE,TRUE}Booleen;

//STRUCTURE
typedef struct
{
	int idArticle;
	float prix;
	int qte;
	char libelle[100];
	
}Article;

typedef struct maillonArticle
{
	Article a;
	struct maillonArticle *suiv;

}MaillonArticle,*ListeArticle;

typedef struct
{
	int idClient;
	char civil[6];
	char nom[100];
	char prenom[100];
	char adresse[100];

}Client;

typedef struct
{
	int idCmd;
	int idArticle;
	int idClient;
	int qteNonL;

}Commande;

typedef struct
{
	char fileNameArticles[50];
	char fileNameClients[50];
	char fileNameCommandes[50];
	char BINfileNameArticles[50];
	char BINfileNameClients[50];
	char BINfileNameCommandes[50];
	Booleen startUpAnimation;
	Booleen displayImage;
	
}Config;


//Menu
void dispMenu(void);
void dispMenuChargement(void);
void dispMenuAffichage(void);
void dispMenuSave(void);
char choixMenu(void);
char choixMenuChargement(void);
char choixMenuAffichage(void);
char choixMenuSave(void);

//Ascii
void dispStart(void);
void alexaJeNaiPasComprisVotreRequete(void);

//Config
Config loadConfigFile(void);
Config editConfig(Config cfg);
int saveConfig(Config cfg);

//Articles
Article lire1Article(FILE *flot);
void afficher1Article(Article a);
void afficherNArticle(ListeArticle l);
ListeArticle chargementNArticle(ListeArticle l,char fileName[]);
ListeArticle chargementNArticleBIN(ListeArticle l,char fileName[]);
ListeArticle chargementNArticleBIN(ListeArticle l,char fileName[]);
void affichageArticleRupture(ListeArticle listeArticle);
void afficherArticleGraphique(ListeArticle listeArticle,Config cfg);
void afficherNArticleGraphique(ListeArticle listeArticle,Config cfg);
ListeArticle ajouter1Article(ListeArticle listeArticle);
void arrivageDeStock(ListeArticle listeArticle,Commande **tabCmd,int *nbCommande);

//Clients
Client lire1Client(FILE *flot);
void afficher1Client(Client c);
void afficherNClient(Client *tabClient[],int n);
int chargementNClient(Client *tabClient[],int tMax,char fileName[]);
int chargementNClientBIN(Client *tabClient[],int tMax,char fileName[]);
void ajouterClient(Client *tabClient[],int *nbClient,int maxCli,Client cli);
Client newClient(int nbClient);
void creationNewClient(Client *tabClient[],int *nbClient,int maxCli);
int rechercherIdClientIndice(Client *tabClient[],int nbClient,int idClientRech,int *trouve);
void supprimerClientMenu(Client *tabClient[],int *nbClient,Commande **tabCommande,int *nbCommande);
void supprimer1Client(Client *tabClient[], int *nbClient,int indice);
void getAdresse(char *adresse);
void modifierAdresseClient(Client *tabClient[],int nbClient);

//Commandes
Commande lire1Commande(FILE *flot);
void afficher1Commande(Commande c);
void afficherNCommande(Commande **tabCmd,int n);
Commande** chargementNCommande(int *nbCommande,char fileName[]);
Commande** chargementNCommandeBIN(int *nbCommande,char fileName[]);
Commande ** insererCommande(Commande **tabCmd,int *nbCommande,Commande cmdAInserer);
void resumeCommande(Commande cmd);
Commande** newCommande(int *nbCommande,int *nbClient,int maxCli,Client *tabClient[],ListeArticle listeArticle,Commande **tabCmd);
int rechercherIndiceCommandeParIdArticle(Commande **tabCmd,int nbCommande,int idArticle);
int rechercherIndiceCommandeParIdClient(Commande **tabCmd,int nbCommande,int idClient);
void supprimerCommande(Commande **tabCommande, int *nbCommande,int indice);
int diminuerQteNonLivree(Commande **tabCmd,int *nbCommande,int idArticleArr,int qteArr);

//Tri
void triDicoCommande(Commande **tabCommande,int nbCommande);
void copier(Commande **tabCommande,int n,int m,Commande **RS);
void fusion(Commande **R,int n,Commande **S,int m,Commande **tabCommande);
void testTriListeArticle(ListeArticle listeArticle);
Booleen listeArticleTrieHomeMade(ListeArticle listeArticle);

//Gestion des Stocks
void lignesCommandesPourClient(Client *tabClient[],int nbClient,Commande **tabCommande,int nbCommande,ListeArticle listeArticle);
int afficherCommandePour1Client(int idClient,Commande **tabCommande,int nbCommande,ListeArticle listeArticle);
void affichageClientsAvecCommande(Client *tabClient[],int nbClient,Commande **tabCommande,int nbCommande,ListeArticle listeArticle);
int partieClientNewCommande(Client *tabClient[],int *nbClient,int maxCli);
Commande partieArticleNewCommande(ListeArticle listeArticle,int idClient,int *nbCommande);
float calculPrixCmd(float prix,int qte);
void resumeClient(Client *tabClient[],Commande **tabCommande,int nbCommande,ListeArticle listeArticle,int indiceClient);
void resumeNClient(Client *tabClient[],int nbClient,Commande **tabCommande,int nbCommande,ListeArticle listeArticle);
int keyDetec(void);



//Sauvegarde
void saveArticle(char fileName[],ListeArticle listeArticle);
void saveClient(char fileName[],Client *tabClient[],int nbClient);
void saveCommande(char fileName[],Commande **tabCommande,int nbCommande);
void saveArticleBin(char fileName[],ListeArticle listeArticle);
void saveClientBin(char fileName[],Client *tabClient[],int nbClient);
void saveCommandeBin(char fileName[],Commande **tabCommande,int nbCommande);

//Fonctions Listes
ListeArticle newListArticle(void);
ListeArticle insererEnTete(ListeArticle l,Article a);
void freeList(ListeArticle l);
Booleen rechercherIdArticle(ListeArticle listeArticle,int idArticle);
char* envoieLibelle(ListeArticle listeArticle,int idArticle);
float envoiePrix(ListeArticle listeArticle,int idArticle);
int envoieQuantite(ListeArticle listeArticle,int idArticle);
void reduirLeStockArticle(ListeArticle listeArticle,int idArticle,int reduction);
void augmenterLeStockArticle(ListeArticle listeArticle,int idArticle,int augmentation);
int longueurListe(ListeArticle l);
ListeArticle getNemeArticle(ListeArticle listeArticle,int i);



//Fonctions String
void toUpperCase(char chaine[]);
void toUpperCaseFirstLetter(char chaine[]);
