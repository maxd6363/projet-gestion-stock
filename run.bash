#!/bin/bash

rm ./projet &>/dev/null
gcc *.c -Wall -Wextra -o ./debug/projet

if test -e debug/projet
then

if test "$1" == 'new'
	then
		xfce4-terminal -T "Gestion des Stocks" --maximize --hide-scrollbar --hide-menubar --geometry 300x300 --fullscreen -e 'bash -c "./debug/projet; bash"' 
	else

	./debug/projet
fi

else
	echo "Erreur gcc"
fi


