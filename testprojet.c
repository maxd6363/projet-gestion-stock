/*
testprojet.c
Maxime	POULAIN
Nicolas	SELVY
Marius	CHANDEZE

Projet A1 Janvier 2019
*/
#include "projet.h"

void gestionStock(void){

	//Variables & Pointeurs pour chargement des données en memoire centrale
	int nbCommandes, nbClient;
	ListeArticle listArticle;
	Client *tabClient[MAXCLI];
	Commande **tabCmd;
	Config  cfg;

	//Variables & Pointeurs généraux
	char choixPrincipal,choixChargement,choixAffichage,choixSave;
	int i;

	cfg=loadConfigFile();
	if(cfg.startUpAnimation==1){
		dispStart();
		alexaJeNaiPasComprisVotreRequete();
	}

	fflush(stdout);
	fflush(stdin);

	listArticle=newListArticle();
	listArticle=chargementNArticle(listArticle,cfg.fileNameArticles);
	if(listArticle==NULL){
		printf("Erreur chargement article\n");
		return;
	}

	nbClient=chargementNClient(tabClient,MAXCLI,cfg.fileNameClients);
	if(nbClient==-1){
		printf("Erreur chargement clients\n");
		return;
	}

	tabCmd=chargementNCommande(&nbCommandes,cfg.fileNameCommandes);
	if(tabCmd==NULL){
		printf("Erreur chargement commandes\n");
		return;
	}

	triDicoCommande(tabCmd,nbCommandes);
	testTriListeArticle(listArticle);
	
	choixPrincipal=choixMenu();
	while(choixPrincipal!='9'){
		//Menu principale
		switch(choixPrincipal){
			case '1':
				//Menu Chargement
			choixChargement=choixMenuChargement();
			while(choixChargement!='9'){
				switch(choixChargement){
					case '1':
					freeList(listArticle);
					listArticle=newListArticle();

					listArticle=chargementNArticle(listArticle,cfg.fileNameArticles);
					if(listArticle==NULL){
						printf(RED "\nErreur chargement article\n" RESET);
					}
					else{
						printf(GRE "\nChargement articles effectué avec succès\n" RESET);
						testTriListeArticle(listArticle);
					}
					scanf("%*c");
					break;
					case '2':
					nbClient=chargementNClient(tabClient,MAXCLI,cfg.fileNameClients);
					if(nbClient==-1){
						printf(RED "\nErreur chargement clients\n" RESET);
						return;
					}
					else{
						printf(GRE "\nChargement clients effectué avec succès\n" RESET);
					}
					scanf("%*c");
					break;
					case '3':
					tabCmd=chargementNCommande(&nbCommandes,cfg.fileNameCommandes);
					if(tabCmd==NULL){
						printf(RED "\nErreur chargement commandes\n" RESET);
						return;
					}
					else{
						printf(GRE "\nChargement commandes effectué avec succès\n" RESET);
						triDicoCommande(tabCmd,nbCommandes);
					}
					scanf("%*c");
					break;
					case '4':
					listArticle=chargementNArticleBIN(listArticle,cfg.BINfileNameArticles);
					if(listArticle==NULL){
						printf(RED "\nErreur chargement article\n" RESET);
					}
					else{
						printf(GRE "\nChargement articles binaire effectué avec succès\n" RESET);
					}
					scanf("%*c");
					break;
					case '5':
					nbClient=chargementNClientBIN(tabClient,MAXCLI,cfg.BINfileNameClients);
					if(nbClient==-1){
						printf(RED "\nErreur chargement clients\n" RESET);
					}
					else{
						printf(GRE "\nChargement clients binaire effectué avec succès\n" RESET);
						testTriListeArticle(listArticle);
					}
					scanf("%*c");
					break;
					case '6':
					tabCmd=chargementNCommandeBIN(&nbCommandes,cfg.BINfileNameCommandes);
					if(tabCmd==NULL){
						printf(RED "\nErreur chargement commandes\n" RESET);
					}
					else{
						printf(GRE "\nChargement commandes binaire effectué avec succès\n" RESET);
						triDicoCommande(tabCmd,nbCommandes);
					}
					scanf("%*c");
					break;
					case '7':
					if(saveConfig(cfg)==0){
						printf(GRE "Vous devez relancer l'application pour appliquer les changements\n" RESET);
					}
					else{
						printf(RED "Erreur chargement configuration\n" RESET);
					}
					scanf("%*c");
					break;
				}
				choixChargement=choixMenuChargement();
			}			
			break;

			case '2':
				//Menu Affichage
			choixAffichage=choixMenuAffichage();
			while(choixAffichage!='9'){
				switch(choixAffichage){
					case '1':
					system("clear");
					printf(YEL "Articles : \n\n" RESET);
					printf("Article\t Prix\tQuantité\tLibellé\n");
					afficherNArticle(listArticle);
					scanf("%*c");
					break;
					case '2':
					system("clear");
					afficherNArticleGraphique(listArticle,cfg);
					scanf("%*c");
					break;
					case '3':
					system("clear");
					printf(YEL "Articles en rupture de stock : \n\n" RESET);
					printf("Article\t Prix\tQuantité\tLibellé\n\n");
					affichageArticleRupture(listArticle);
					scanf("%*c");
					break;
					case '4':
					system("clear");
					afficherNClient(tabClient,nbClient);
					scanf("%*c");
					break;
					case '5':
					system("clear");
					resumeNClient(tabClient,nbClient,tabCmd,nbCommandes,listArticle);
					scanf("%*c");
					break;
					case '6':
					system("clear");
					affichageClientsAvecCommande(tabClient,nbClient,tabCmd,nbCommandes,listArticle);
					scanf("%*c");
					break;
					case '7':
					system("clear");
					lignesCommandesPourClient(tabClient,nbClient,tabCmd,nbCommandes,listArticle);
					scanf("%*c");
					break;
					case '8':
					system("clear");
					afficherNCommande(tabCmd,nbCommandes);
					scanf("%*c");
					break;
				}
				choixAffichage=choixMenuAffichage();
			}
			break;
			case '3':
			system("clear");
			tabCmd=newCommande(&nbCommandes,&nbClient,MAXCLI,tabClient,listArticle,tabCmd);
			scanf("%*c");
			break;
			case '4':
			system("clear");
			ajouter1Article(listArticle);
			scanf("%*c");
			break;
			case '5':
			system("clear");
			arrivageDeStock(listArticle,tabCmd,&nbCommandes);
			scanf("%*c");
			break;
			case '6':
			system("clear");
			creationNewClient(tabClient,&nbClient,MAXCLI);
			scanf("%*c");
			break;
			case '7':
			system("clear");
			supprimerClientMenu(tabClient,&nbClient,tabCmd,&nbCommandes);
			scanf("%*c");
			break;
			case '8':
			system("clear");
			modifierAdresseClient(tabClient,nbClient);
			scanf("%*c");
			break;
			case 'A':
				//Menu sauvegarde
			choixSave=choixMenuSave();
			while(choixSave!='9'){
				switch(choixSave){
					case '1':
					printf("\n");
					saveArticle(cfg.fileNameArticles,listArticle);
					scanf("%*c");
					break;
					case '2':
					printf("\n");
					saveClient(cfg.fileNameClients,tabClient,nbClient);
					scanf("%*c");
					break;
					case '3':
					printf("\n");
					saveCommande(cfg.fileNameCommandes,tabCmd,nbCommandes);
					scanf("%*c");
					break;
					case '4':
					printf("\n");
					saveArticleBin(cfg.BINfileNameArticles,listArticle);
					scanf("%*c");
					break;
					case '5':
					printf("\n");
					saveClientBin(cfg.BINfileNameClients,tabClient,nbClient);
					scanf("%*c");
					break;
					case '6':
					printf("\n");
					saveCommandeBin(cfg.BINfileNameCommandes,tabCmd,nbCommandes);
					scanf("%*c");
					break;
					case '7':
					cfg=editConfig(cfg);
					scanf("%*c");
					break;
				}
				choixSave=choixMenuSave();
			}
			break;
		}
		choixPrincipal=choixMenu();
	}
	printf("\n");
	saveArticle(cfg.fileNameArticles,listArticle);
	saveClient(cfg.fileNameClients,tabClient,nbClient);
	saveCommande(cfg.fileNameCommandes,tabCmd,nbCommandes);

	printf( BLINK "Bye\n" RESET);


	freeList(listArticle);

	for(i=0;i<nbClient;i++){
		free(tabClient[i]);
	}
	for(i=0;i<nbCommandes;i++){
		free(tabCmd[i]);
	}
}

int main(void){
	gestionStock();
	return 0;
}
